Function DrawButton(txt As String, x As Integer, y As Integer, xwidth As Integer, yheight As Integer, fcolor As UInteger = fbBlack, fbordercolor As UInteger = fbWhite, ftextcolor As UInteger = fbGray) As Button
	y -= 8
	xwidth -= 1
	'''''''''Inner lines'''''''''
	For i As Integer = y To y + yheight
		Line (x , i) - (x + xwidth, i), fcolor
	Next

	'''''''''Outer lines'''''''''
	'upper line
	Line (x, y) - (x + xwidth, y), fbordercolor
	'bottom
	Line(x, y + yheight) - (x + xwidth, y + yheight), fbordercolor
	'left
	Line (x, y) - (x, y + yheight), fbordercolor
	'right
	Line (x + xwidth, y) - (x + xwidth, y + yheight), fbordercolor
	' Transposer �a (ici pour le centre de l'�cran
	' pour le centre du bouton
	'((txcols / 2) - (Len(ct) / 2))
	'((txrows / 2) - (ni / 2))
	Dim xcenter As Integer = x + (xwidth / 2) - (Len(txt) * charw) / 2
	Dim ycenter As Integer = ((yheight / 2) - 4) + y
	Draw String (xcenter, ycenter), txt, ftextcolor
	Dim As Button returnbutton
	With returnbutton
		.xpos = x
		.ypos = y
		.xwidth = xwidth
		.yheight = yheight
		.text = txt
	End With
	Return returnbutton
End Function

Function CheckButtonPress(button As Button) As Integer
	Dim As Integer mouseposx, mouseposy
	GetMouse(mouseposx, mouseposy)
	If button.xpos <= mouseposx And mouseposx <= (button.xpos + button.xwidth) And button.ypos <= mouseposy And mouseposy <= (button.ypos + button.yheight) Then
		Return TRUE
	Else
		Return FALSE
	EndIf
End Function