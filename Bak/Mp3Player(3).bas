'' mp3 player based on FMOD

#Include "fbgfx.bi"
#If __FB_LANG__ = "fb"
Using FB '' Scan code constants are stored in the FB namespace in lang FB
#EndIf

'
' Add a button system !
'
' Add a vizualizer
'
' NEED TRANSPARENCY !
'
'
' Oscilloscope
'
'
'

Dim Shared As String ckey, moretext
Dim Shared As String tag1, tag2, tag3, dtag1, dtag2, dtag3

#Include "defines.bi"
#Include "utils.bi"
#Include Once "fmod.bi"
#Include "file.bi"
#Include "buttons.bi"

moretext = version

ScreenRes SWidth, SHeight, 32
Width txcols, txrows
WindowTitle "MP3Player"

' Open the file for FMOD
Dim SOUND_FILE As String
If FileExists("test.mp3") Then
	SOUND_FILE = "test.mp3"
ElseIf FileExists("test.wav") Then
	SOUND_FILE = "test.wav"
Else
	Print "Impossible d'ouvrir le fichier ! (test.mp3/test.wav)"
	Sleep
	End
EndIf

ReDim Shared As String tagsaname(1)
ReDim Shared As String tagsainfo(1)

Sub print_all_tags(ByVal stream As FSOUND_STREAM Ptr)
	Dim As Integer count = 0
	FSOUND_Stream_GetNumTagFields(stream, @count)
	For i As Integer = 0 To (count - 1)
		Dim As Integer tagtype, taglen
		Dim As ZString Ptr tagname, tagvalue
		FSOUND_Stream_GetTagField(stream, i, @tagtype, @tagname, @tagvalue, @taglen)
		ReDim Preserve tagsaname(UBound(tagsaname) + 1)
		ReDim Preserve tagsainfo(UBound(tagsainfo) + 1)
		tagsaname(i) = Left(*tagname, taglen)
		'tagsainfo(i) = Left(*tagtype, taglen)
		tagsainfo(i) = tagsainfo(i) & "|" & Left(*tagvalue, taglen)
		tagsainfo(i) = tagsainfo(i) & "  |" & Left(Str(taglen), taglen)
	Next
End Sub

Function get_tag _
	( _
	ByVal stream As FSOUND_STREAM Ptr, _
	ByVal tagv1 As ZString Ptr, _
	ByVal tagv2 As ZString Ptr _
	) As String

	Dim tagname As ZString Ptr, taglen As Integer

	FSOUND_Stream_FindTagField(stream, FSOUND_TAGFIELD_ID3V1, tagv1, @tagname, @taglen)
	If (taglen = 0) Then
		FSOUND_Stream_FindTagField(stream, FSOUND_TAGFIELD_ID3V2, tagv2, @tagname, @taglen)
	End If

	Return Left(*tagname, taglen)
End Function

If (FSOUND_GetVersion < FMOD_VERSION) Then
	Print "FMOD version " + Str(FMOD_VERSION) + " or greater required!"
	End 1
End If

If (FSOUND_Init(44100, 4, 0) = 0) Then
	Print "Could not initialize FMOD"
	End 1
End If

FSOUND_Stream_SetBufferSize(50)

Dim As FSOUND_STREAM Ptr stream = FSOUND_Stream_Open(SOUND_FILE, FSOUND_MPEGACCURATE, 0, 0)
If (stream = 0) Then
	Print "FMOD could not load '" & SOUND_FILE & "'"
	Sleep
	FSOUND_Close()
	End 1
End If

'' Read out mp3 tags to show some meta information
tag1 = "Title:"
dtag1 = get_tag(stream, "TITLE", "TIT2")
tag2 = "Album:"
dtag2 = get_tag(stream, "ALBUM", "TALB")
tag3 = "Artist:"
dtag3 = get_tag(stream, "ARTIST", "TPE1")
print_all_tags(stream)
FSOUND_Stream_Play(FSOUND_FREE, stream)
' End of the FMOD things

Dim Shared As Integer mtime, mmaxtime, ispaused = 0, savedtime, volume
Dim Shared As String currenttime, totaltime
Dim Shared As Integer mmousex, mmousey, mmousebutton
mmaxtime = FSOUND_Stream_GetLengthMs(stream)
totaltime = Str(Int(mmaxtime / 1000))
volume = FSOUND_GetSFXMasterVolume()

Dim Shared As Integer mlastmousex, mlastmousey, mlastmousebutton
Dim Shared As Integer positiontime, positionvol
Dim Shared As Integer lastx1, lastx2

'Between	x= 15 To 55
'			y= 60 To 60

Dim Shared volmax As Any Ptr

volmax = ImageCreate(16, 16, fbWhite)

BLoad "volmax.bmp", volmax

'Buttons
Dim Shared As Button restartmusicbutton, freqbutton

Sub DrawScreen()
	rowcount = 1
	ScreenLock
	DrawBackground(fbSlateGrayDark)
	If dtag1 <> "" Then PutTextShadow(tag1, rowcount, 1)
	If dtag1 <> "" Then PutText(dtag1, rowcount, 15)
	rowcount += 1
	If dtag2 <> "" Then PutTextShadow(tag2, rowcount, 1)
	If dtag2 <> "" Then PutText(dtag2, rowcount, 15)
	rowcount += 1
	If dtag3 <> "" Then PutTextShadow(tag3, rowcount, 1)
	If dtag3 <> "" Then PutText(dtag3, rowcount, 15)
	restartmusicbutton = DrawButton("Restart", txcols * charw - 75 - charw * 2, rowcount * charh + 10, 75, 22)
	rowcount += 3
	PutTextShadow("Position", rowcount, 1)
	PutText(currenttime, rowcount, 16, fbFlame)
	PutText(" / " & totaltime, rowcount, 15 + Len(currenttime))
	DrawProgressbarRect(colstart * charw, rowcount * charh + borderup, colend * charw, rowcount * charw + borderup, 8 + borderdown, mtime, mmaxtime, axisy, fbFlame, fbGray)
	positiontime = rowcount
	rowcount += 5
	PutTextShadow("Volume", rowcount, 1)
	PutText(Int(volume * 100 / 255) & "%", rowcount, 15)
	DrawProgressbarRect(colstart * charw, rowcount * charh + borderup, colend * charw, rowcount * charw + borderup, 8 + borderdown, volume, 255, axisy, fbSteelBlue, fbGray)
	positionvol = rowcount
	rowcount += 5
	For i As Integer = 0 To UBound(tagsaname) - 1
		PutTextShadow(tagsaname(i), rowcount, 1)
		PutText(tagsainfo(i), rowcount, 10)
		rowcount += 1
	Next
	freqbutton = DrawButton(Str(FSOUND_GetFrequency(FSOUND_FREE) & " | " & Str(FSOUND_GetErrorString())), 90, 276, 50, 50)
	PutTextBottom("x:" & mlastmousex & " y:" & mlastmousey & " | " & moretext)
	'Put (10, 10), volmax, Trans
	ScreenUnlock
End Sub

Do
	ckey = Inkey
	If ckey <> "" Then
		Select Case ckey
			Case key_up
		End Select
	EndIf
	GetMouse(mlastmousex, mlastmousey, ,mlastmousebutton)
	'check x and y mousepos for time
	If mlastmousebutton = 1 Then
		'Check if "Load" button is pressed
		If CheckButtonPress(restartmusicbutton) Then
			mtime = 0
			FSOUND_Stream_SetTime(stream, mtime)
		ElseIf mlastmousey <= positiontime * charh + borderup + 8 + borderdown And mlastmousey >= positiontime * charh + borderup And mlastmousex >= colstart * charw And mlastmousex <= colend * charw And mlastmousebutton = 1 Then
			While mlastmousebutton = 1
				GetMouse(mlastmousex, mlastmousey, ,mlastmousebutton)
			Wend
			If mlastmousex >= colend * charw Then mlastmousex = colend * charw
			If mlastmousex <= colstart * charw Then mlastmousex = colstart * charw
			mtime = (((mlastmousex - (colstart * charw)) * mmaxtime) / ((colend * charw) - (colstart * charw)))
			FSOUND_Stream_SetTime(stream, mtime)
		ElseIf mlastmousey <= (6 + 2) * charh + borderdown And mlastmousey >= 6 * charh + borderup And mlastmousebutton = 1 Then

			'check x and y mousepos for volume
		ElseIf mlastmousey <= positionvol * charh + borderdown + 8 + borderdown And mlastmousey >= positionvol * charh + borderup And mlastmousex >= colstart * charw And mlastmousex <= colend * charw And mlastmousebutton = 1 Then
			While mlastmousebutton = 1
				GetMouse(mlastmousex, mlastmousey, ,mlastmousebutton)
				If mlastmousex >= colend * charw Then mlastmousex = colend * charw
				If mlastmousex <= colstart * charw Then mlastmousex = colstart * charw
				volume = (((mlastmousex - (colstart * charw)) * 255) / ((colend * charw) - (colstart * charw)))
				If volume = -1 Then volume = 255
				FSOUND_SetSFXMasterVolume(volume)
				DrawScreen()
			Wend
		ElseIf mlastmousey <= (10 + 2) * charh + borderdown And mlastmousey >= 10 * charh + borderup And mlastmousebutton = 1 Then

		EndIf
	EndIf
	mtime = FSOUND_Stream_GetTime(stream) 'return an integer in ms
	currenttime = Str(Int(mtime / 1000))

	'Check for different keys pressed
	If MultiKey(SC_UP) Or MultiKey(SC_PLUS) And volume < 255 Then
		volume += 1
		FSOUND_SetSFXMasterVolume(volume)
	ElseIf MultiKey(SC_DOWN) Or MultiKey(SC_MINUS) Then
		If volume > 0 Then
			volume -= 1
			FSOUND_SetSFXMasterVolume(volume)
		EndIf
	ElseIf MultiKey(SC_LEFT) And mtime > 500 Then
		mtime -= 500
		FSOUND_Stream_SetTime(stream, mtime)
	ElseIf MultiKey(SC_RIGHT) And mtime < mmaxtime - 500 Then
		mtime += 500
		FSOUND_Stream_SetTime(stream, mtime)
	ElseIf MultiKey(SC_SPACE) Then
		While MultiKey(SC_SPACE)
		Wend
		If ispaused = 0 Then
			ispaused = 1
			savedtime = mtime
			FSOUND_Stream_Stop(stream)
			savedtime = mtime
		ElseIf ispaused = 1 Then
			ispaused = 0
			FSOUND_Stream_Play(FSOUND_FREE, stream)
			FSOUND_Stream_SetTime(stream, savedtime)
		EndIf
	EndIf

	'Check if music finished
	If (FSOUND_Stream_GetPosition(stream) >= FSOUND_Stream_GetLength(stream)) Then
		FSOUND_Stream_SetTime(stream, 0)
	End If

	'Put a black background then re-draw every thing
	DrawScreen()
Loop Until ckey = key_esc Or ckey = key_close

FSOUND_Stream_Stop(stream)
FSOUND_Stream_Close(stream)
FSOUND_Close()
End