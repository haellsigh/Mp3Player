Sub DrawBackground(fbcolor As UInteger = fbBlack)
	Dim As Integer x, y
	For x = 0 To txcols - 1
		For y = 0 To txrows - 1
			Draw String (x * charw, y * charh), acBlock, fbcolor
		Next
	Next
End Sub

'Splits text InS at sLen and returns clipped string.
Function WordWrap(InS As String, sLen As Integer) As String
	Dim As Integer i = sLen, sl
	Dim As Integer BackFlag = FALSE
	Dim As String sret, ch

	'Make sure we have something to work with here.
	sl = Len(InS)
	If sl <= sLen Then
		sret = InS
		InS = ""
	Else
		'Find the break point in the string, backtracking
		'to find a space to break the line at if not at a space.
		Do
			'Break is at space, so done.
			ch = Mid(InS, i, 1)
			If ch = Chr(32) Then
				Exit Do
			End If
			'If not backtracking, start backtrack.
			If BackFlag = FALSE Then
				If i + 1 <= sl Then
					i+= 1
				End If
				BackFlag = TRUE
			Else
				i -= 1
			End If
		Loop Until i = 0 Or ch = Chr(32) 'Backtrack to space.
		'Make sure we still have something to work with.
		If i > 0 Then
			'Return clipped string.
			sret = Mid(InS, 1, i)
			'Modify the input string: string less clipped.
			InS = Mid(InS, i + 1)
		Else
			sret = ""
		End If
	End If
	Return sret
End Function

'Writes text at specified row and column.
Sub PutText(txt As String, row As Integer, col As Integer, fcolor As UInteger = fbWhite)
	Dim As Integer ptrow = 0
	Dim As String txt1, txt0 = txt
	Dim As Integer x, y
	x = (col - 1) * charw
	y = (row - 1) * charh

	'if txt is longer than space remaining
	If Len(txt0) > (txcols - col) Then
		Do While Len(txt0) > 0
			txt1 = WordWrap(txt0, (txcols - col) - 2)
			Draw String (x, y + (ptrow * 8)), txt1, fcolor
			ptrow += 1
			rowcount += 1
		Loop
	Else
		Draw String (x, y), txt0, fcolor
	EndIf
End Sub

'Writes text at the bottom, at the specified column.
Sub PutTextBottom(txt As String, fcolor As UInteger = fbWhite)
		Draw String (0, (txcols * charh) - charh), txt, fcolor
End Sub

'Writes text at specified row and column with shadow.
Sub PutTextShadow(txt As String, row As Integer, col As Integer, fcolor As UInteger = fbWhite)
	Draw String ((row * charh) + 1, (col * charw) + 1), txt, fbBlack
	PutText(txt, row, col, fcolor)
End Sub

Sub LineCol(col1 As Integer, row1 As Integer, col2 As Integer, row2 As Integer, largeur As Integer, axis As Integer = axisx, fcolor As UInteger = fbWhite)
	If largeur < 1 Then largeur = 1
	largeur -= 1
	Dim i As Integer
	If axis = axisx Then
		For i = 0 To largeur
			Line((charw * col1) + i, charh * row1) - ((charw * col2) + i, charh * row2), fcolor
		Next
	ElseIf axis = axisy Then
		For i = 0 To largeur
			Line(charw * col1, (charh * row1) + i) - (charw * col2 , (charh * row2) + i), fcolor
		Next
	EndIf
End Sub

Sub LineAxis(x1 As Integer, y1 As Integer, x2 As Integer, y2 As Integer, largeur As Integer, axis As Integer = axisx, fcolor As UInteger = fbWhite)
	If largeur < 1 Then largeur = 1
	largeur -= 1
	Dim i As Integer
	If axis = axisx Then
		For i = 0 To largeur
			Line((x1 + i), y1) - ((x2 + i), y2), fcolor
		Next
	ElseIf axis = axisy Then
		For i = 0 To largeur
			Line(x1, (y1 + i)) - (x2, (y2 + i)), fcolor
		Next
	EndIf
End Sub

Sub DrawProgressbarRect(x1 As Integer, y1 As Integer, x2 As Integer, y2 As Integer, largeur As Integer, progress As Integer, totalprogress As Integer, axis As Integer = axisx, fcolor As UInteger = fbWhite, fbordercolor As UInteger = fbRed)
	If progress > totalprogress Then progress = totalprogress
	If progress < 0 Then progress = 0
	'(x2 - x1) = longueur en pixel du rectangle
	LineAxis(x1, y1, x1 + (x2 - x1) * progress / totalprogress, y2, largeur, axis, fcolor)
	'haut
	Line (x1, y1) - (x2, y2), fbordercolor
	'bas
	Line (x1, y1 + largeur) - (x2, y2 + largeur), fbordercolor
	'gauche
	Line (x1, y1) - (x1, y2 + largeur), fbordercolor
	'droite
	Line (x2, y1) - (x2, y2 + largeur), fbordercolor
End Sub