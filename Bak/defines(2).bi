#Ifndef FALSE
#Define FALSE 0
#EndIf
#Ifndef TRUE
#Define TRUE -1
#EndIf

#Define version "Version 1.0.1b (buttons)"

#Define ColToPx(col) (col * charw)

'Screen size
Const SWidth = 50 * 8, SHeight = 75 * 8

'Text mode 80x60
#Define txcols SWidth / 8
#Define txrows SHeight / 8

'Characters size
#Define charw 8
#Define charh 8

'Macro that calculates the center point on the screen.
#Define CenterX(ct) ((txcols / 2) - (Len(ct) / 2))
#Define CenterY(ni) ((txrows / 2) - (ni / 2))

'Progressbar bounds
#Define colstart 2
#Define colend (txcols - colstart)
#Define borderup 5
#Define borderdown 5

'Define if the axis
#Define axisx 0
#Define axisy 1

'General row count ! (reset at every redraw)
Dim Shared As Integer rowcount = 1

'Key consts
Const acBlock = Chr(219)
Const xk = Chr(255)
Const key_up = xk + "H"
Const key_do = xk + "P"
Const key_ri = xk + "M"
Const key_le = xk + "K"
Const key_close = xk + "k"
Const key_esc = Chr(27)
Const key_enter = Chr(13)

'Colors
Const fbYellow = RGB(200, 200, 0)
Const fbYellowBright = RGB(255, 255, 0)
Const fbWhite = RGB(255, 255, 255)
Const fbWhite1 = RGB(200, 200, 200)
Const fbWhite2 = RGB(150, 150, 150)
Const fbWhite3 = RGB(100, 100, 100)
Const fbBlack = RGB(0, 0, 0)
Const fbGray = RGB(128, 128, 128)
Const fbTan = RGB (210, 180, 140)
Const fbSlateGrayDark = RGB (47, 79, 79)
Const fbGreen = RGB (0, 200, 0)
Const fbRed = RGB (200, 0, 0)
Const fbRedBright = RGB (255, 0, 0)
Const fbSienna = RGB (160, 082, 045)
Const fbGold = RGB (255, 215, 000)
Const fbSalmon = RGB (250, 128, 114)
Const fbHoneydew = RGB (240, 255, 240)
Const fbYellowGreen = RGB (154, 205, 050)
Const fbSteelBlue = RGB (70, 130, 180)
Const fbCadmiumYellow = RGB (255, 153, 18)
Const fbOrange = RGB (255, 128, 0)
Const fbMagenta = RGB (255, 0, 255)
Const fbSilver = RGB(192, 192, 192)
Const fbPink = RGB (255, 192, 203)
Const fbBlue = RGB (0, 0, 255)
Const fbCyan = RGB (0, 255, 255)
Const fbTurquoise = RGB (064, 224, 208)
Const fbFlame = RGB(226, 88, 34)

'Types

'The button type
'contains xpos, ypos, xwidth, yheight
Type Button
	xpos As Integer
	ypos As Integer
	xwidth As Integer
	yheight As Integer
	text As String
End Type